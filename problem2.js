const fs = require('fs');
const path = require("path"); 


function readingfile(path)
{

   return new Promise(function (resolve,reject)
    {
        fs.readFile(path, (err,data) => {
            if(err)
            {
                reject(err);
            }
            else
            {
                
                resolve(data);
                
            }
        }); //read
    });//promise
}// func




function writingfile(path, data)
{

  return new Promise(function (resolve,reject){
    fs.writeFile(path, data, err => {
        if(err){
            reject(err);
        }

        else
        {
          resolve("file written");
        }
        
    });//write
  });//promise
}


function appendingfile(path, data)
{
  return new Promise(function (resolve,reject){
  fs.appendFile(path, data, (err) => {
    if (err) {
      reject(err);
    }

    else
    resolve("data appended");
    
  }); //append
}); //promise
}

function removefiles(arr)
{
  return new Promise(function (resolve,reject){
            arr.forEach(file => {
                var pathway= path.resolve(__dirname, file);
                fs.unlink(pathway, (err) => {
                    if(err){
                        reject(err);
                    }
                  
                    
                });//unlink


            });//foreach

            resolve("Files removed");
          });//promise

            

}


function f1(data)
{
  var read2= data.toString();
  var read3= read2.toLowerCase();
  var read4= read3.split(".");  //read4 has null string in the end
   

   var read5= read4.slice(0,read4.length-1);

   for (let i = 0; i < read5.length; i++) {
    read5[i] = read5[i].trim();
  }



   for (var i = 0; i < read5.length; i++)  
    {
      read5[i] = read5[i].charAt(0).toUpperCase() + read5[i].substring(1); 
    }

     var read6="";

     for (var i = 0; i < read5.length; i++)
     {
       read6 = read6 + read5[i] + "."
     }

     return read6;



}


function sort(data)
{

  var read6= data.toString(); 

   var read7= read6.split(".");

   for (let i = 0; i < read7.length-1; i++) {
     read7[i] = read7[i].trim();
   }
 var read8= read7.slice(0,read7.length-1);

   read8.sort();


   var read9="";

  for (var i = 0; i < read8.length; i++)
  {
    read9 = read9 + read8[i] + ".";
  }

  return read9;


}


function makeArray(data)
{
  var str= data.toString();
  var arr= str.split(".txt");
  console.log(arr);
  var arr1= arr.slice(0, arr.length-1);

  for(var i=0;i<arr1.length;i++)
  {
    arr1[i]= arr1[i]+".txt";
  }
  return arr1;
}










//------------------------------------------------------------------------------------------------------------



function problem2()
{
    

 readingfile(path.resolve(__dirname, "lipsum.txt"))
 .then ((data)=> {                                                   //this data is of readingfile promise
   const upper= data.toString().toUpperCase();
   return writingfile(path.resolve(__dirname, "newFile1.txt"),upper);  //returning promise of writingfile
              })


.then ((data)=>{                                                    //this data is of writingfile promise
  console.log(data);
  return writingfile(path.resolve(__dirname, "filenames.txt"),"newFile1.txt");  //returning promise of writingfile
})


.then ((data)=>{      //this data is of writingfile promise
  console.log(data);
 return readingfile(path.resolve(__dirname, "newFile1.txt"))   //returning promise of readingfile
 
})

.then ((data)=>{   //this data is of readingfile promise

var data1= f1(data);
return writingfile(path.resolve(__dirname, "newFile2.txt"),data1);  //returning promise of writingfile

})

.then ((data)=>{   //this data is of writingfile promise

  console.log(data);
  return appendingfile(path.resolve(__dirname, "filenames.txt"),"newFile2.txt");  //returning promise of appendingfile
  
  })

  

.then ((data)=>{   //this data is of appendingfile promise

  console.log(data);
  return readingfile(path.resolve(__dirname, "newFile1.txt"));  //returning promise of readingfile
  
  })



  .then ((data)=>{   //this data is of readingfile promise

  
   var data1= sort(data);

   return writingfile(path.resolve(__dirname, "final.txt"),data1);  //returning promise of writingfile
   
    
    })



  .then ((data)=>{   //this data is of writingfile promise

    console.log(data);
    return readingfile(path.resolve(__dirname, "newFile2.txt"));  //returning promise of readingfile
  })


.then ((data)=>{   //this data is of readingfile promise

  
   var data1= sort(data);

   return appendingfile(path.resolve(__dirname, "final.txt"),data1);  //returning promise of writingfile
   
    
    })

    .then ((data)=>{   //this data is of writingfile promise
      console.log(data);
      return appendingfile(path.resolve(__dirname, "filenames.txt"),"final.txt");  //returning promise of appendingfile
      
       
       })


.then ((data)=>{   //this data is of appendingfile promise

  console.log(data);
  return readingfile(path.resolve(__dirname, "filenames.txt"));  //returning promise of readingfile
      
  })

  
.then ((data)=>{   //this data is of readingfile promise

  var arr= makeArray(data);
  return removefiles(arr);  //returning promise of removefiles
      
  })

  .then ((data)=>{   //this data is of removefiles promise

    console.log(data);
    
      })



  








  
 }// func



module.exports= problem2;














