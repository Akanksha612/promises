


const fs = require("fs");
const path = require("path");


function createDirectory(dir){
  return new Promise ((resolve, reject) => {
    fs.mkdir(dir, (err) => {
      if(err){
        reject(err)
      }else{
        console.log('New Directory Created')
        resolve()
      }
    });//mkdir
  })//promise
}


function writeFile(path, data)
{
  
    return new Promise((resolve,reject)=> {

        fs.writeFile(path, data, err => {
            if(err){
                reject(err);
            }
            else{
                console.log("File created");
                resolve();
            }
            
        });//write
    });//promise


}



function removeFiles(dir)
{
    return new Promise((resolve,reject)=> {


    fs.readdir(dir, (err, data) => {
        if(err){
            reject(err);
        }
        else{
            data.forEach(file => {
                fs.unlink(dir + file, (err) => {
                    if(err){
                        reject(err);
                    }

                    else
                    {
                        console.log("File deleted");
                    }
                });//unlink
            });//loop
            
    resolve();
            
        }
    });//readdir

});//promise
}              


function problem1(){
    let data = 'random text';
    const dir = __dirname + '/json/'; 

    // next "then" par tab aayega jab previous "then" promise return karega.
    //means returned promise ka resolve() call hua hai aur flow then mei gya hai.

    createDirectory(dir)
    .then(() => {return writeFile(dir+'file1.json',data)})  
    .then(() => {return writeFile(dir +'file2.json',data)})  
    .then(() => {return writeFile(dir +'file3.json',data)})
    .then(() => {return removeFiles(dir)});

  }

module.exports= problem1;


